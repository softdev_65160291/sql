/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.connect;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class Connect {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:C:/Code/Softdev/Database/Week 1/Database/dcoffee.db";
        
        try{
            conn = DriverManager.getConnection(url);
             System.out.println("Database Connected");
        } catch (SQLException ex ){
            System.out.println(ex.getMessage());
            return;
         }
        
        String sql = "SELECT * FROM category";
        try {       
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                System.out.println(rs.getInt("cat_id") + " " + rs.getString("cat_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (conn != null) {
            try {
                    conn.close();

                } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    }

