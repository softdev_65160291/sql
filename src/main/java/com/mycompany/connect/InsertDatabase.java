/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.connect;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InsertDatabase {
        public static void main(String[] args) {
            
        Connection conn = null;
        String url = "jdbc:sqlite:C:/Code/Softdev/Database/Week 1/Database/dcoffee.db";
        
        try{
            conn = DriverManager.getConnection(url);
             System.out.println("Database Connected");
        } catch (SQLException ex ){
            System.out.println(ex.getMessage());
            return;
         }
        
        
        String sql = "INSERT INTO category(cat_id, cat_name) VALUES (3,'candy' )";
        try {       
            Statement stmt = conn.createStatement();
            int status = stmt.executeUpdate(sql);
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (conn != null) {
            try {
                    conn.close();

                } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
